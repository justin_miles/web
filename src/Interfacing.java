import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

public class Interfacing {
	
	private static String DEFAULT_PROMPT = ">>>";
	
	private static Scanner sc = new Scanner(System.in);
		
	public static ArrayList<String> handleUserInputType(String message){
		return handleUserInputType(message, false);
	}
	
	public static ArrayList<String> handleUserInputType(boolean autobox){
		return handleUserInputType(DEFAULT_PROMPT, autobox);
	}
	
	public static ArrayList<String> handleUserInputType(String message, boolean autobox) {
		String input = null;
		int secondsToWait = autobox ? 0 : 7;
		final ExecutorService es = Executors.newSingleThreadExecutor();
		try{
			final Future<String> f = es.submit(() -> {
				return getUserInput(message);
			});
            input = f.get(secondsToWait, TimeUnit.SECONDS);
        } catch (final TimeoutException e) {
            input = JOptionPane.showInputDialog(message);
        } catch (final Exception e) {
            throw new RuntimeException(e);
        } finally {
            es.shutdown();
        }
		return inputParser(input);
	}
	
	private static ArrayList<String> inputParser(String input){
		ArrayList<String> cmdParams = new ArrayList<String>();
		//single or double quoted strings into an array
		Pattern regex = Pattern.compile("[^\\s\"']+|\"([^\"]*)\"|'([^']*)'");
		Matcher regexMatcher = regex.matcher(input);
		while (regexMatcher.find()) {
		    if (regexMatcher.group(1) != null) {
		        // Add double-quoted string without the quotes
		    	cmdParams.add(regexMatcher.group(1));
		    } else if (regexMatcher.group(2) != null) {
		        // Add single-quoted string without the quotes
		    	cmdParams.add(regexMatcher.group(2));
		    } else {
		        // Add unquoted word
		    	cmdParams.add(regexMatcher.group());
		    }
		} 
        return cmdParams;		
	}
	

//	private static String getUserInput() {
//		return getUserInput(DEFAULT_PROMPT);
//	}

	private static String getUserInput(String message) {
		System.out.println(message);
		return sc.nextLine();
	}

}
