import java.io.IOException;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.Priority;
import org.apache.log4j.RollingFileAppender;

public class Log {
	/**
	 * Ideas: able to change file location of log at runtime
	 */

	private Logger LOG;
	
	public void log(Priority priority, String message){
		if(null == LOG){
			setuplogger();
		}
		LOG.log(priority, message);
	}
	
	private void setuplogger() {
		setuplogger("log.txt");
	}
	
	private void setuplogger(String filename){
		LOG = Logger.getRootLogger();
		String pattern = "";
		try {
			LOG.addAppender(new RollingFileAppender(new PatternLayout(pattern), filename));
			LOG.addAppender(new ConsoleAppender(new PatternLayout(pattern), ConsoleAppender.SYSTEM_OUT));
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("bad file name");
			setuplogger(Interfacing.handleUserInputType(false).get(0));
		}
	}

}
