
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.security.Credentials;
import org.openqa.selenium.security.UserAndPassword;
import org.apache.http.client.CredentialsProvider;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Architecture;
import org.openqa.selenium.Beta;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.ContextAware;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.HasCapabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.Rotatable;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//continue to explore at the packages within this

import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
import com.thoughtworks.selenium.Selenium;
import com.thoughtworks.selenium.webdriven.commands.SeleniumSelect;
import com.thoughtworks.selenium.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public class Robo {

	static WebDriver driver;
	static Selenium selenium;
	static Log LOG = new Log();

	public static void main(String[] args) {
		
		System.getProperties().forEach((s, y) -> System.out.println(s + " " + y));
		System.out.println("===========================================");
		System.getenv().forEach((s, y) -> System.out.println(s + " " + y));

	}
	public static void mine(String[] args){
		boolean isConsole = args[0].equalsIgnoreCase("console");
		
        By by = By.ByCssSelector.cssSelector("");
        String url = "";
        String name = "";
        String engine = "";
        String logType = "";
        long time = 6;
        TimeUnit unit = TimeUnit.DAYS;
        Point targetPosition = new Point(0, 0);
        Dimension targetSize = new Dimension(0, 0);
        Credentials credentials = (Credentials) new DefaultCredentialsProvider();
        String keysToSend = "";
        String index = "";
        String nameOrId = "";
        String frameElement = "";
        String nameOrHandle = "";
        String className = "";
        String selector = "";
        String id = "";
        String linkText = "";
        String xpathExpression = "";

        DesiredCapabilities dc = new DesiredCapabilities();
        // TODO Auto-generated method stub
        driver = new ChromeDriver(dc);
        ArrayList<String> input = Interfacing.handleUserInputType("");
        
        String cmd = input.get(0);
        input.remove(0);
        ArrayList<String> params = input;
        
        while (!cmd.equalsIgnoreCase("exit")) {
            switch (cmd) {
                case "driver": 
                	driver(params);
                    break;
                case "manage": 
                    manage(params);
                    break;
                case "ime": 
                    ime(params);
                    break;
                case "logs": 
                    logs(params);
                    break;
                case "timeouts": 
                    timeouts(params);
                    break;
                case "window": 
                    window(params);
                    break;
                case "navigate": 
                    navigate(params);
                    break;
                case "switchTo": 
                    switchTo(params);
                    break;
                case "alert": 
                    alert(params);
                    break;
                case "defaultContent": 
                    defaultContent(params);
                    break;
                case "by":
                    by(params);
                default:
                	help(params);
            }
        }
        driver.quit();//closes all windows
    }
	
	private static void help() {
		help(null);
	}
	
	private static void help(ArrayList<String> params){
		
	}

	public static Object driver(ArrayList<String> params) {
		driver.close();// closes the current open window
//		driver.findElement(by);
//		driver.findElements(by);
//		driver.get(url);
		driver.getCurrentUrl();
		driver.getPageSource();
		driver.getTitle();
		driver.getWindowHandle();
		driver.getWindowHandles();
		return null;
	}

	/**
	 * make a class member list so object returned by this type of method can be accessed by getting the list's last object
	 * @param params
	 * @return
	 */
	public static Set<Cookie> manage(ArrayList<String> params) {
		switch(params.get(0)){
			case "addCookie":
				driver.manage().addCookie(new Cookie(params.get(1), params.get(2), params.get(3), params.get(4), new Date(params.get(5)), params.get(6).equalsIgnoreCase("true"), params.get(7).equalsIgnoreCase("true")));
				break;
			case "deleteAllCookies":
				driver.manage().deleteAllCookies();
				break;
			case "deleteCookie":
				driver.manage().deleteCookie(new Cookie(params.get(1), params.get(2), params.get(3), params.get(4), new Date(params.get(5)), params.get(6).equalsIgnoreCase("true"), params.get(7).equalsIgnoreCase("true")));
				break;
			case "getCookieNamed":
				Set<Cookie> set = new HashSet<>();
				set.add(driver.manage().getCookieNamed(params.get(1)));
				return set;
			case "getCookies":
				return driver.manage().getCookies();
			default:
				help(stringToArray("manage"));
		}
		return null;
	}

	public static void ime(ArrayList<String> params) {
		switch(params.get(0)){
			case "activateEngine":
			driver.manage().ime().activateEngine(params.get(1));
				break;
			case "deactivate":
				driver.manage().ime().deactivate();
				break;
			case "getActiveEngine":
				driver.manage().ime().getActiveEngine();
				break;
			case "getAvailableEngines":
				driver.manage().ime().getAvailableEngines();
				break;
			case "isActivated":
				driver.manage().ime().isActivated();
				break;
			default:
				help(stringToArray("ime"));
		}
	}

	public static LogEntries logs(ArrayList<String> params) {
		for(String type : driver.manage().logs().getAvailableLogTypes()){
			if(params.get(0).equalsIgnoreCase(type)){
				return driver.manage().logs().get(type);	
			}
		}
		help(stringToArray("logs"));
		return null;
	}

	public static void timeouts(ArrayList<String> params) {
		int time = Integer.parseInt(params.get(1));
		TimeUnit unit = TimeUnit.valueOf(params.get(2));
		switch(params.get(0)){
			case "implicitWait":
				driver.manage().timeouts().implicitlyWait(time, unit);
				break;
			case "pageLoadTimeout":
				driver.manage().timeouts().pageLoadTimeout(time, unit);
				break;
			case "setScriptTimeout":
				driver.manage().timeouts().setScriptTimeout(time, unit);
				break;
			default:
				help(stringToArray("timeouts"));
		}
	}

	public static void window(ArrayList<String> params) {
		switch(params.get(0)){
			case "getPosition":
				driver.manage().window().getPosition();
				break;
			case "getSize":
				driver.manage().window().getSize();
				break;
			case "maximize":
				driver.manage().window().maximize();
				break;
			case "setPosition":
				driver.manage().window().setPosition(new Point(Integer.parseInt(params.get(1)), Integer.parseInt(params.get(2))));
				break;
			case "setSize":
				driver.manage().window().setSize(new Dimension(Integer.parseInt(params.get(1)), Integer.parseInt(params.get(2))));
				break;
			default:
				help(stringToArray("window"));
		}
	}

	public static void navigate(ArrayList<String> params) {
		switch(params.get(0)){
			case "back":
				driver.navigate().back();
				break;
			case "forward":
				driver.navigate().forward();
				break;
			case "refresh":
				driver.navigate().refresh();
				break;
			case "to":
				driver.navigate().to(params.get(1));
				break;
			default:
				help(stringToArray("navigate"));
		}
	}
	
	public static void switchTo(ArrayList<String> params) {
		switch(params.get(0)){
			case "activeElement":
		driver.switchTo().activeElement();
				break;
			case "frame":
				try{
					int index = Integer.parseInt(params.get(1));
					driver.switchTo().frame(index);
				}catch(NumberFormatException e){
					if(params.get(1).contains("#") || params.get(1).contains(".")){
						driver.switchTo().frame("");
					}else{
						driver.switchTo().frame(driver.findElement(By.xpath("")));
					}
				}
				break;
			case "parentFrame":
				driver.switchTo().parentFrame();
				break;
			case "window":
				driver.switchTo().window(params.get(1));
				break;
			default:
				help(stringToArray("switchTo"));
		}
	}

	public static void alert(ArrayList<String> params) {
		switch(params.get(0)){
			case "accept":
				driver.switchTo().alert().accept();
				break;
			case "authenticate":
				String username = params.get(1);
				String password = params.get(2);
				driver.switchTo().alert().authenticateUsing((Credentials) getCredententials(params));
				break;
			case "dismiss":
				driver.switchTo().alert().dismiss();
				break;
			case "getText":
				driver.switchTo().alert().getText();
				break;
			case "sendKeys":
				driver.switchTo().alert().sendKeys(params.get(1));
				break;
			default:
				help(stringToArray("alert"));
		}
	}
	
	private static DefaultCredentialsProvider getCredententials(ArrayList<String> credentials){
		DefaultCredentialsProvider cred = new DefaultCredentialsProvider();
		if(credentials.size() == 2){
			cred.addCredentials(credentials.get(0), credentials.get(1));
		}else if(credentials.size() == 5){
			cred.addCredentials(credentials.get(0), 
								credentials.get(1), 
								credentials.get(2), 
								Integer.parseInt(credentials.get(3)), 
								credentials.get(4));
		}else if(credentials.size() == 6){
			cred.addNTLMCredentials(credentials.get(0), 
									credentials.get(1), 
									credentials.get(2), 
									Integer.parseInt(credentials.get(3)), 
									credentials.get(4),
									credentials.get(5));
		}
		return cred;
	}
	
	private static ArrayList<String> stringToArray(String string){
		return (ArrayList<String>)Arrays.asList(new String[]{string});
	}

	public static void defaultContent(ArrayList<String> params) {
		driver.switchTo().defaultContent().close();
	}

	public static By by(ArrayList<String> params) {
		String cmd = params.get(0);
		switch (cmd) {
			case "id":
				return By.id(cmd);
			case "class":
				return By.className(cmd);
			case "linktext":
				return By.linkText(cmd);
			case "name":
				return By.name(cmd);
			case "tag":
				return By.tagName(cmd);
			case "xpath":
				return By.xpath(cmd);
			default:
				return By.cssSelector(cmd);
		}
	}
}
